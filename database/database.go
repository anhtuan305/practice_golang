package database

import (
	"demo/common"
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type SqlParam struct {
	Host     string
	Port     int
	Password int
	Database string
	UserName string
}

var DB *gorm.DB

func DbInit() (*gorm.DB, error) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	sql := SqlParam{}
	sql.Host = os.Getenv("DB_HOST")
	sql.Database = os.Getenv("DB_DATABASE")
	sql.UserName = os.Getenv("DB_USERNAME")
	sql.Port = common.Convert_String_To_Int(os.Getenv("DB_PORT"))
	sql.Password = common.Convert_String_To_Int(os.Getenv("DB_PASSWORD"))

	return InitPostgres(sql)
}

func InitPostgres(sql SqlParam) (*gorm.DB, error) {
	url := fmt.Sprintf("host=%s port=%d user=%s "+"password=%d dbname=%s sslmode=disable", sql.Host, sql.Port, sql.UserName, sql.Password, sql.Database)
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	DB = db
	return DB, nil
}

func GetDB() *gorm.DB {
	return DB
}
