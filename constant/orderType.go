package constant

const (
	SortName             = "a to z"
	ReversedSortName     = "z to a"
	SortQuantity         = "low to high"
	ReversedSortQuantity = "high to low"
	SortPrice            = "low to high"
	ReversedSortPrice    = "high to low"
)
