package config

import (
	"demo/database"
	model "demo/models"

	"gorm.io/gorm"
)

func DefaultInit() {
	db, error := database.DbInit()
	//sqlDB, _ := db.DB()
	//defer sqlDB.Close()
	MigrateDatabase(db)
	if error != nil {
		panic(error.Error())
	}
}

func MigrateDatabase(db *gorm.DB) {
	db.AutoMigrate(&model.AdminModel{})
	db.AutoMigrate(&model.CartModel{})
	db.AutoMigrate(&model.CategoryModel{})
	db.AutoMigrate(&model.ProductCategory{})
	db.AutoMigrate(&model.OrderModel{})
	db.AutoMigrate(&model.ProductModel{})
	db.AutoMigrate(&model.OrderDetailModel{})
	db.AutoMigrate(&model.UserModel{})
	//db.AutoMigrate(&models.CartProductModel{})
}
