package models

import (
	"demo/database"
	"errors"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserModel struct {
	gorm.Model

	Phone    string `gorm:"column:phone;not null"`
	Username string `gorm:"column:username;not null"`
	Password string `gorm:"column:password;not null"`
	Email    string `gorm:"column:email;not null"`
	Birthday string `gorm:"column:birthday;not null"`
	Gender   string `gorm:"column:gender"`
	Address  string `gorm:"column:address"`
	Picture  string `gorm:"column:picture"`
	IsOnline bool   `gorm:"column:is_online"`
	Cart     CartModel
}

func (u *UserModel) SetPassword(password string) error {
	if len(password) == 0 {
		return errors.New("password should not be empty!")
	}
	bytePassword := []byte(password)
	Password, _ := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	u.Password = string(Password)
	return nil
}

func SaveOne(data interface{}) error {
	db := database.GetDB()
	err := db.Save(data).Error
	db.Session(&gorm.Session{FullSaveAssociations: true}).Updates(&data)
	return err
}

func SendResetPassword(data interface{}) (UserModel, error) {
	db := database.GetDB()
	var model UserModel
	err := db.Debug().Where("email = ?", data).Error
	// err := db.Updates(data).Error
	// return err
	return model, err
}

func FindOneUser(condition interface{}) (UserModel, error) {
	db := database.GetDB()
	var usermodel UserModel
	err := db.Where("email", condition).First(&usermodel).Error
	return usermodel, err
}

func (u *UserModel) CheckPassword(password string) error {
	bytePassword := []byte(password)
	byteHashedPassword := []byte(u.Password)
	return bcrypt.CompareHashAndPassword(byteHashedPassword, bytePassword)
}
