package models

import "gorm.io/gorm"

type OrderDetailModel struct {
	gorm.Model

	Price         uint64 `gorm:"column:price;not null"`
	Quantity      uint64 `gorm:"column:quantity;not null"`
	OrdersModel   []OrderModel
	ProductsModel []ProductModel
}
