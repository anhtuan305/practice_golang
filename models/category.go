package models

import (
	constant "demo/constant"
	"demo/database"
	"strconv"

	"gorm.io/gorm"
)

type CategoryModel struct {
	gorm.Model

	Name            string `gorm:"column:name;not null"`
	Description     string `gorm:"column:description;not null"`
	Thumbnail       string `gorm:"column:thumbnail;not null"`
	ProductCategory []ProductCategory
	//Products    []ProductModel `gorm:"many2many:product_categories"`
	Quantity string `gorm:"column:quantity;not null"`
}

func ListAllCategory(orderType string) ([]CategoryModel, error) {
	var condition string
	var flag bool
	switch orderType {
	case constant.SortName:
		condition = "asc"
		flag = true
	case constant.ReversedSortName:
		condition = "desc"
		flag = true
	case constant.SortQuantity:
		condition = "asc"
		flag = false
	case constant.ReversedSortQuantity:
		condition = "desc"
		flag = false
	}
	if flag {
		categorymodel, err := getCategoryByName(condition)
		return categorymodel, err
	} else if !flag {
		categorymodel, err := getCategoryByQuantity(condition)
		return categorymodel, err
	} else {
		categorymodel, err := getCategoryWithoutFilter()
		return categorymodel, err
	}
}

func getCategoryByName(condition string) ([]CategoryModel, error) {
	db := database.GetDB()
	var categorymodel []CategoryModel
	err := db.Order("name " + condition).Find(&categorymodel).Error
	return categorymodel, err
}

func getCategoryByQuantity(condition string) ([]CategoryModel, error) {
	db := database.GetDB()
	var categorymodel []CategoryModel
	err := db.Order("quantity " + condition).Find(&categorymodel).Error
	return categorymodel, err
}
func getCategoryWithoutFilter() ([]CategoryModel, error) {
	db := database.GetDB()
	var categorymodel []CategoryModel
	err := db.Find(&categorymodel).Error
	return categorymodel, err
}
func GetCategoryDetail(id string) CategoryModel {
	db := database.GetDB()
	newId, _ := strconv.Atoi(id)
	var categorymodel CategoryModel
	db.Find(&categorymodel, "id = ? ", newId)
	return categorymodel
}
