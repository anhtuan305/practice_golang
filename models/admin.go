package models

import (
	"database/sql"

	"gorm.io/gorm"
)

type AdminModel struct {
	gorm.Model

	Username string       `gorm:"column:username;not null"`
	Password string       `gorm:"column:password;not null"`
	IsOnline sql.NullBool `gorm:"column:is_online"`
}
