package models

import (
	constant "demo/constant"
	"demo/database"
	"strconv"

	"gorm.io/gorm"
)

type ProductModel struct {
	gorm.Model

	Name            string `gorm:"column:name;not null"`
	RemainingAmount string `gorm:"column:remaining_amount;not null"`
	Price           string `gorm:"column:price;not null"`
	Weight          string `gorm:"column:weight;not null"`
	Description     string `gorm:"column:description;not null"`
	Thumbnail       string `gorm:"column:thumbnail;not null"`
	CreateDate      string `gorm:"column:create_date;not null"`
	//Categories         []CategoryModel `gorm:"many2many:product_categories"`
	ProductCategory    []ProductCategory
	Carts              []CartModel `gorm:"many2many:cart_product"`
	OrderDetailModelID uint64      `gorm:"column:order_detail_id;unique;not null;autoIncrement:true"`
}

func ListProduct(orderType, sortBy string, CategoryID int) ([]ProductCategory, error) {
	var condition string
	var flag bool
	var products []ProductCategory
	var err error
	switch orderType {
	case constant.SortName:
		condition = "asc"
		flag = true
	case constant.ReversedSortName:
		condition = "desc"
		flag = true
	}
	switch sortBy {
	case constant.SortPrice:
		condition = "asc"
		flag = false
	case constant.ReversedSortPrice:
		condition = "desc"
		flag = false
	}
	if flag {
		products = getProductByName(condition, CategoryID)
	}
	return products, err
}

func getProductByName(condition string, CategoryID int) []ProductCategory {
	var products []ProductCategory
	db := database.GetDB()
	db.Where("category_id = ?", CategoryID).Preload("Product").Preload("Category").Find(&products)
	return products
}

func GetProductDetail(id string) ProductModel {
	db := database.GetDB()
	newID, _ := strconv.Atoi(id)
	var product ProductModel
	db.Find(&product, "id = ? ", newID)
	return product
}
