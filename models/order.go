package models

import (
	"time"

	"gorm.io/gorm"
)

type OrderModel struct {
	gorm.Model

	Amount             uint64    `gorm:"column:amount;not null"`
	UserID             uint64    `gorm:"column:user_id;not null"`
	OrderAddress       string    `gorm:"column:order_address;not null"`
	OrderMail          string    `gorm:"column:order_mail;not null"`
	OrderDate          time.Time `gorm:"column:order_date;not null"`
	PaymentDate        time.Time `gorm:"column:payment_date;not null"`
	CompletedDate      time.Time `gorm:"column:completed_date;not null"`
	CartID             uint64    `gorm:"column:cart_id;not null"`
	PaymentType        string    `gorm:"column:payment_type;not null"`
	OrderDetailModelID uint64    `gorm:"column:order_detail_id;unique;not null"`
}
