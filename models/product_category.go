package models

import (
	"demo/database"

	"gorm.io/gorm"
)

type ProductCategory struct {
	gorm.Model
	ProductModelID  uint64 `gorm:"column:product_id;not null"`
	CategoryModelID uint64 `gorm:"column:category_id;not null"`
	Product         ProductModel
	Category        CategoryModel
}

func CreateProductModelID(ProductID, CategoryID uint) error {
	db := database.GetDB()
	productCategory := ProductCategory{ProductModelID: uint64(ProductID), CategoryModelID: uint64(CategoryID)}
	err := db.Save(&productCategory).Error
	return err
}
