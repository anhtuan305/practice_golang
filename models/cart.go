package models

import (
	database "demo/database"

	"gorm.io/gorm"
)

type CartModel struct {
	gorm.Model

	UserModelID   uint64         `gorm:"column:user_id`
	ProductsModel []ProductModel `gorm:"many2many:cart_product"`
}

// type CartProductModel struct {
// 	ProductID uint `gorm:"column:product_id"`
// 	CartID    uint `gorm:"column:cart_id"`
// 	Count     uint `gorm:"column:count"`
// }

func (model *CartModel) SetProduct(products []string) error {
	db := database.GetDB()
	var productList []ProductModel
	for _, product := range products {
		var productModel ProductModel
		err := db.FirstOrCreate(&productModel, ProductModel{Name: product}).Error
		if err != nil {
			return err
		}
		productList = append(productList, productModel)
	}
	model.ProductsModel = productList
	return nil
}

func ListItem() (ProductModel, error) {
	db := database.GetDB()
	var product ProductModel
	err := db.Debug().Model(&product).Preload("Carts").Find(&product).Error
	//err := db.Debug().Model(&product).Association("Carts").Find(&product)
	return product, err
}

// func DeleteItem(id string) error {
// 	id_int, _ := strconv.Atoi(id)
// 	db := database.GetDB()
// 	err := db.Where("id = ?", id).Delete()

// }
