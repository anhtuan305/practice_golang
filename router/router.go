package router

import (
	"demo/service"

	"github.com/gin-gonic/gin"
)

func RouterInit() *gin.Engine {
	r := gin.Default()
	v1 := r.Group("/api/v1")
	{
		user := v1.Group("/user")
		{
			user.POST("/sign_up", service.UserRegistration)
			user.POST("/sign_in", service.UserLogin)
			user.PUT("/forgot_password", service.UserForgotPassword)
		}
		// // image := v1.Group("/image")
		// // {
		// // 	image.POST()
		// // }
	}
	//service.User(v1.Group("/user"))
	// v1.POST("")
	// service.Category(v1.Group("/category"))
	// service.Image(v1.Group("/image"))
	// service.Product(v1.Group("/product"))
	// service.Cart(v1.Group("/cart"))
	// r.StaticFS("/file", http.Dir("file"))
	return r
}
