package main

import (
	config "demo/config"
	router "demo/router"
)

func main() {
	config.DefaultInit()
	r := router.RouterInit()
	r.Run(":8082")
}
