package request

import (
	common "demo/common"
	model "demo/models"

	"github.com/gin-gonic/gin"
)

type CategoryCreation struct {
	Category struct {
		Thumbnail   string `json:"thumbnail"`
		Quantity    string `json:"quantity"`
		Name        string `json:"name"`
		Description string `json:"description"`
	} `json:"category"`
	CategoryModel   model.CategoryModel   `json:"-"`
	ProductCategory model.ProductCategory `json:"-"`
}

func NewCategoryModelValidator() CategoryCreation {
	categorycreation := CategoryCreation{}
	return categorycreation
}

func (self *CategoryCreation) Bind(c *gin.Context) error {
	err := common.Bind(c, self)
	if err != nil {
		return err
	}
	self.CategoryModel.Thumbnail = self.Category.Thumbnail
	self.CategoryModel.Quantity = self.Category.Quantity
	self.CategoryModel.Name = self.Category.Name
	self.CategoryModel.Description = self.Category.Description
	self.ProductCategory.CategoryModelID = uint64(self.CategoryModel.ID)
	return nil
}
