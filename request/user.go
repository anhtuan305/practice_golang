package request

import (
	common "demo/common"
	"demo/models"

	"github.com/gin-gonic/gin"
)

type UserRegistration struct {
	User struct {
		Phone    string `json:"phone" validate:"required,unique"`
		Username string `json:"username"`
		Password string `json:"password" validate:"required,gte=6"`
		Email    string `json:"email" validate:"required,email"`
		Birthday string `json:"birthday" validate:"required"`
		Gender   string `json:"gender" validate:"required"`
		Address  string `json:"address"`
		Picture  string `json:"picture"`
		//IsOnline bool   `json:"is_online"`
		//Cart     string `json:"cart" validate:"required,unique"`
	} `json:"user"`
	UserModel models.UserModel `json:"-"`
}

type UserLogin struct {
	User struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	} `json:"user"`
	UserModel models.UserModel `json:"-"`
}

type UserForgotPassword struct {
	User struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	} `json:"user"`
	UserModel models.UserModel `json:"-"`
}

func NewUserModelValidator() UserRegistration {
	userRequest := UserRegistration{}
	return userRequest
}

func NewLoginValidator() UserLogin {
	loginValidator := UserLogin{}
	return loginValidator
}

func NewForgotPasswordValidator() UserForgotPassword {
	UserForgotPassword := UserForgotPassword{}
	return UserForgotPassword
}

func (self *UserRegistration) Bind(c *gin.Context) error {
	err := common.Bind(c, self)
	if err != nil {
		return err
	}
	self.UserModel.Phone = self.User.Phone
	self.UserModel.Username = self.User.Username
	self.UserModel.Email = self.User.Email
	self.UserModel.Gender = self.User.Gender
	self.UserModel.Address = self.User.Address
	self.UserModel.Picture = self.User.Picture
	//self.UserModel.IsOnline = self.User.IsOnline
	self.UserModel.Birthday = common.Convert_String_To_Date(self.User.Birthday)
	//self.UserModel.SetCart(self.User.Cart)
	self.UserModel.SetPassword(self.User.Password)
	return nil
}

func (self *UserForgotPassword) Bind(c *gin.Context) error {
	err := common.Bind(c, self)
	if err != nil {
		return err
	}
	self.UserModel.Email = self.User.Email
	self.UserModel.Password = self.User.Password
	return nil
}

func (self *UserLogin) Bind(c *gin.Context) error {
	err := common.Bind(c, self)
	if err != nil {
		return err
	}
	self.UserModel.Email = self.User.Email
	self.UserModel.IsOnline = true
	return nil
}
