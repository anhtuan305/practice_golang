package request

import (
	common "demo/common"
	model "demo/models"

	"github.com/gin-gonic/gin"
)

type ProductCreation struct {
	Product struct {
		Name            string `json:"name"`
		Price           string `json:"price"`
		Weight          string `json:"weight"`
		Description     string `json:"description"`
		Thumbnail       string `json:"thumbnail"`
		RemainingAmount string `json:"remaining_amount"`
		CreatedDate     string `json:"created_date"`
		CategoryID      uint64 `json:"category_id"`
	} `json:"product"`
	ProductModel    model.ProductModel    `json:"-"`
	ProductCategory model.ProductCategory `json:"-"`
}

func NewProductModelValidator() ProductCreation {
	productcreation := ProductCreation{}
	return productcreation
}

func (self *ProductCreation) Bind(c *gin.Context) error {
	err := common.Bind(c, self)
	if err != nil {
		return err
	}
	self.ProductModel.Name = self.Product.Name
	self.ProductModel.Price = self.Product.Price
	self.ProductModel.Thumbnail = self.Product.Thumbnail
	self.ProductModel.Weight = self.Product.Weight
	self.ProductModel.Description = self.Product.Description
	self.ProductModel.RemainingAmount = self.Product.RemainingAmount
	self.ProductModel.CreateDate = self.Product.CreatedDate
	return nil
}
