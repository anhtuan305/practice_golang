package request

import (
	common "demo/common"
	model "demo/models"

	"github.com/gin-gonic/gin"
)

type CartCreation struct {
	Cart struct {
		UserModelID uint64   `json:"userId"`
		ProductList []string `json:"productList"`
	} `json:"cart"`
	CartModel model.CartModel `json:"-"`
}

func NewCartModelValidator() CartCreation {
	cartcreation := CartCreation{}
	return cartcreation
}

func (self *CartCreation) Bind(c *gin.Context) error {
	err := common.Bind(c, self)
	if err != nil {
		return err
	}
	self.CartModel.UserModelID = self.Cart.UserModelID
	self.CartModel.SetProduct(self.Cart.ProductList)
	return nil
}
