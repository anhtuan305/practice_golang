package request

import "gorm.io/gorm"

type AdminRequest struct {
	gorm.Model

	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required,gte=6"`
	IsOnline bool   `json:"is_online"`
}
