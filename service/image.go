package service

import (
	"demo/common"
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func Image(router *gin.RouterGroup) {
	router.POST("/upload", UploadImage)

}
func UploadImage(c *gin.Context) {
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(http.StatusBadGateway, common.NewValidatorError(err))
		return
	}
	filename := header.Filename
	out, err := os.Create("./file/" + filename)
	if err != nil {
		c.JSON(http.StatusInternalServerError, common.NewValidatorError(err))
		return
	}
	defer out.Close()
	_, err = io.Copy(out, file)
	if err != nil {
		c.JSON(http.StatusInternalServerError, common.NewValidatorError(err))
		return
	}
	filepath := "http://localhost:8082/file/" + filename
	c.JSON(http.StatusOK, gin.H{"filepath": filepath})
}
