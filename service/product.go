package service

import (
	common "demo/common"
	model "demo/models"
	"strconv"

	request "demo/request"
	response "demo/response"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Product(router *gin.RouterGroup) {
	router.POST("/create", ProductCreate)
	router.GET("/list", ProductList)
	router.GET("/:id", ProductGetDetail)
}

func ProductCreate(c *gin.Context) {
	productCreate := request.NewProductModelValidator()
	if err := productCreate.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	if err := model.SaveOne(&productCreate.ProductModel); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("Database", err))
		return
	}
	if err := model.CreateProductModelID(productCreate.ProductModel.ID, uint(productCreate.Product.CategoryID)); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("Database", err))
		return
	}
	model.SaveOne(&productCreate.ProductCategory.ProductModelID)
	c.Set("product_model", productCreate.ProductModel)
	productSerializer := response.ProductSerializer{Context: c}
	c.JSON(http.StatusCreated, gin.H{"product": productSerializer.Response()})
}

func ProductList(c *gin.Context) {
	orderType := c.Query("orderType")
	categoryID := c.Query("CategoryID")
	sortBy := c.Query("sortBy")
	categoryID_int, _ := strconv.Atoi(categoryID)
	products, _ := model.ListProduct(orderType, sortBy, categoryID_int)
	c.JSON(http.StatusOK, gin.H{"products": products})
}

func ProductGetDetail(c *gin.Context) {
	id := c.Param("id")
	productmodel := model.GetProductDetail(id)
	c.JSON(http.StatusOK, gin.H{"product": productmodel})
}
