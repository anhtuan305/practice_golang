package service

import (
	"demo/common"
	model "demo/models"
	request "demo/request"
	response "demo/response"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Category(router *gin.RouterGroup) {
	router.POST("/create", CategoryCreate)
	router.GET("/list", CategoryList)
	router.GET("/:id", CategoryGetDetail)

}

func CategoryCreate(c *gin.Context) {
	categoryCreate := request.NewCategoryModelValidator()
	if err := categoryCreate.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	if err := model.SaveOne(&categoryCreate.CategoryModel); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("Database", err))
		return
	}
	// if err := model.CreateCategoryModelID(categoryCreate.CategoryModel.ID); err != nil {
	// 	c.JSON(http.StatusUnprocessableEntity, common.NewError("Database", err))
	// 	return
	// }
	model.SaveOne(&categoryCreate.ProductCategory.CategoryModelID)
	c.Set("category_model", categoryCreate.CategoryModel)
	categorySerializer := response.CategorySerializer{Context: c}
	c.JSON(http.StatusCreated, gin.H{"category": categorySerializer.Response()})
}

func CategoryList(c *gin.Context) {
	orderType := c.Query("orderType")
	categorymodel, _ := model.ListAllCategory(orderType)
	c.JSON(http.StatusOK, gin.H{"categories": categorymodel})
}

func CategoryGetDetail(c *gin.Context) {
	id := c.Param("id")
	categorymodel := model.GetCategoryDetail(id)
	c.JSON(http.StatusOK, gin.H{"categories": categorymodel})

}
