package service

import (
	"demo/common"
	model "demo/models"
	request "demo/request"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Cart(router *gin.RouterGroup) {
	router.POST("/create", CreateCart)
	router.GET("/list", CartList)
	//router.DELETE("delete/:id", CartDelete)
}

func CreateCart(c *gin.Context) {
	cartCreate := request.NewCartModelValidator()
	if err := cartCreate.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	if err := model.SaveOne(&cartCreate.CartModel); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("Database", err))
		return
	}
	c.Set("cart_model", cartCreate.CartModel)
	c.JSON(http.StatusOK, "OK")
}

func CartList(c *gin.Context) {
	product, err := model.ListItem()
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("comment", errors.New("Invalid ID")))
		return
	}
	c.JSON(http.StatusOK, gin.H{"product": product})
}

// func CartDelete(c *gin.Context) {
// 	productId := c.Param("id")

// }
