package service

import (
	"demo/common"
	model "demo/models"
	request "demo/request"
	response "demo/response"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

func User(router *gin.RouterGroup) {
	router.POST("/sign_up", UserRegistration)
	router.POST("/sign_in", UserLogin)
	router.PUT("/forgot_password", UserForgotPassword)
}

func UserRegistration(c *gin.Context) {
	userCreate := request.NewUserModelValidator()
	if err := userCreate.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	if err := model.SaveOne(&userCreate.UserModel); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("Database", err))
		return
	}
	c.Set("my_user_model", userCreate.UserModel)
	serializer := response.UserSerializer{Context: c}
	c.JSON(http.StatusCreated, gin.H{"user": serializer.Response()})
	common.SendEmail(userCreate.User.Email)
}

func UserLogin(c *gin.Context) {
	userLogin := request.NewLoginValidator()
	if err := userLogin.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	userModel, err := model.FindOneUser(&(userLogin.UserModel.Email))

	if err != nil {
		c.JSON(http.StatusForbidden, common.NewError("login", errors.New("Not Registered email or invalid password")))
		return
	}
	if userModel.CheckPassword(userLogin.User.Password) != nil {
		c.JSON(http.StatusForbidden, common.NewError("login", errors.New("Not Registered email or invalid password")))
		return
	}
	c.Set("my_user_model", userLogin.UserModel)
	userSerializer := response.UserSerializer{Context: c}
	c.JSON(http.StatusOK, gin.H{"user": userSerializer.Response()})
}

func UserForgotPassword(c *gin.Context) {
	//c.Redirect(http.StatusPermanentRedirect, "https://www.google.com/")
	UserForgotPassword := request.NewForgotPasswordValidator()
	if err := UserForgotPassword.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	if _, err := model.SendResetPassword(UserForgotPassword.UserModel.Email); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("Database", err))
		return
	}
	c.JSON(http.StatusCreated, "OK")

}
