package common

import (
	"strconv"
	"time"
)

func Convert_String_To_Date(birthday string) string {
	num, _ := strconv.ParseInt(birthday, 10, 64)
	day_of_birth := time.Unix(num, 0).Format("02-01-2006")
	return day_of_birth
}
