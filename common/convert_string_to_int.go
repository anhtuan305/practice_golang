package common

import (
	"strconv"
)

func Convert_String_To_Int(s string) int {
	num, _ := strconv.Atoi(s)
	return num
}
