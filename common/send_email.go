package common

import (
	"bytes"
	"fmt"
	"html/template"
	"net/smtp"
)

func SendEmail(receiver string) error {
	sender := "huynhducanhtuan305@gmail.com"
	to := []string{
		receiver,
	}
	password := "yqwlzvmprliikdqn"
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"
	address := smtpHost + ":" + smtpPort

	auth := smtp.PlainAuth("", sender, password, smtpHost)
	t, error := template.ParseFiles("../practice_golang/templateEmail/templateEmail.html")
	if error != nil {
		return error
	}
	var body bytes.Buffer
	mimeHeaders := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	body.Write([]byte(fmt.Sprintf("Subject: Sign In \n%s\n\n", mimeHeaders)))
	t.Execute(&body, struct {
		Link string
	}{
		Link: "http://localhost:8082/api/user/sign_in",
	})
	err := smtp.SendMail(address, auth, sender, to, body.Bytes())
	if err != nil {
		return err
	}
	return nil
}
