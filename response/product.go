package response

import (
	model "demo/models"

	"github.com/gin-gonic/gin"
)

type ProductSerializer struct {
	Context *gin.Context
}

type ProductResponse struct {
	Name            string `json:"name"`
	Thumbnail       string `json:"thumbnail"`
	Price           string `json:"price"`
	Weight          string `json:"weight"`
	Description     string `json:"description"`
	RemainingAmount string `json:"remaningamount"`
	CreateDate      string `json:"create_date"`
}

func (self *ProductSerializer) Response() ProductResponse {
	ProductModel := self.Context.MustGet("product_model").(model.ProductModel)
	product := ProductResponse{
		Thumbnail:       ProductModel.Thumbnail,
		Name:            ProductModel.Name,
		Price:           ProductModel.Price,
		Weight:          ProductModel.Weight,
		Description:     ProductModel.Description,
		RemainingAmount: ProductModel.RemainingAmount,
		CreateDate:      ProductModel.CreateDate,
	}
	return product
}
