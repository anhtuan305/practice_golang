package response

import (
	model "demo/models"

	"github.com/gin-gonic/gin"
)

type CategorySerializer struct {
	Context *gin.Context
}

type CategoryResponse struct {
	Thumbnail   string `json:"thumbnail"`
	Quantity    string `json:"quantity"`
	Name        string `json:"name"`
	Description string `json:"description"`
	ID          uint   `json:"id"`
}

func (self *CategorySerializer) Response() CategoryResponse {
	CategoryModel := self.Context.MustGet("category_model").(model.CategoryModel)
	category := CategoryResponse{
		Thumbnail:   CategoryModel.Thumbnail,
		Quantity:    CategoryModel.Quantity,
		Name:        CategoryModel.Name,
		Description: CategoryModel.Description,
		//	ID:          uint64(CategoryModel.ID),
	}
	return category
}
