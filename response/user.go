package response

import (
	model "demo/models"

	"github.com/gin-gonic/gin"
)

type UserSerializer struct {
	Context *gin.Context
}

type UserResponse struct {
	Phone    string `json:"phone`
	Username string `json:"username"`
	Email    string `json:"email"`
	Birthday string `json:"birthday"`
	Gender   string `json:"gender"`
	Address  string `json:"address"`
	IsOnline bool   `json:"is_online"`
	//CartModel model.CartModel
}

func (self *UserSerializer) Response() UserResponse {
	myUserModel := self.Context.MustGet("my_user_model").(model.UserModel)
	user := UserResponse{
		Phone:    myUserModel.Phone,
		Username: myUserModel.Username,
		Email:    myUserModel.Email,
		Birthday: myUserModel.Birthday,
		Gender:   myUserModel.Gender,
		Address:  myUserModel.Address,
		IsOnline: myUserModel.IsOnline,
		//CartModel: myUserModel.CartModel,
	}
	return user
}
